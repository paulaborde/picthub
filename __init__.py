# -*- coding: utf-8 -*-
from PyQt4.QtGui import *
from threadsPictHub import *
from mainUI import *
import time
from time import strftime
import os
from PyQt4.QtCore import QTimer
from get_FApictures import *
from shutil import copyfile
import subprocess

listePhotos=[]
printedPicture=[]

class interfaceVideo(QDialog,Ui_Dialog):
    """
    IHM du programmme principal
    """
    def __init__(self, parent=None):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.thread = ThreadLectureCamera()
        self.thread.signalAfficherImage.connect(self.afficherUneImage)
        self.thread.start()

        # Connection des boutons
        self.btnShoot.clicked.connect(self.get_picture)
        self.btnPrint.clicked.connect(self.print_stream)
        self.btnPrint_2.clicked.connect(self.print_picture)

        self.btnGalleryMode.clicked.connect(self.gotoGallery)
        self.btnStreamingMode.clicked.connect(self.gotoStreaming)
        #self.btnRefresh.clicked.connect(self.refreshFA)
        self.btnNext.clicked.connect(self.nextGallery)
        self.btnPrevious.clicked.connect(self.previousGallery)

        self.tabWidget.selected.connect(self.tabSelect)


    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_G:
            print("Get picture key")
            self.get_picture()
        if e.key() == QtCore.Qt.Key_P:
            print("Print key")
            if self.tabWidget.currentIndex() == 0:
                self.print_stream()
            elif self.tabWidget.currentIndex() == 1:
                self.print_picture()
        if e.key() == QtCore.Qt.Key_H:
            print("Gallery Mode key")
            self.gotoGallery()
        if e.key() == QtCore.Qt.Key_S:
            print("Streaming Mode key")
            self.gotoStreaming()
        if e.key() == QtCore.Qt.Key_R:
            print("Refresh key")
            self.refreshFA()
        if e.key() == QtCore.Qt.Key_N:
            print("Next key")
            self.nextGallery()
        if e.key() == QtCore.Qt.Key_B:
            print("Back (previous) key")
            self.previousGallery()
        if e.key() == QtCore.Qt.Key_E:
            print("EasterEgg !")
            self.eeg()

    def afficherUneImage(self,img):
        """
        Affichage d'une image dans le QLabel nommé video_frame
        """
        #if len(img.shape)==3: # Si image BGR, la convertir en GRAY
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        height, width, bouzin = img.shape
        #mQImage = QImage(img, width, height, QImage.Format_Indexed8)
        mQImage = QImage(img, width, height, QImage.Format_RGB888)

        pix = QPixmap.fromImage(mQImage)
        #pix = pix.scaled(1366,768)
        pix = pix.scaled(1366,911)

        self.video_frame.setPixmap(pix)

    def get_picture(self):
        print("Shot !")

        # Arret du thread streaming + sauvegarde de l'image
        self.thread.quit()

        name = strftime("%Y-%m-%d_%H-%M-%S") +"_Photobouse.jpg"
        filename = 'pictures/' + name
        pix = self.video_frame.pixmap()
        pix.save(filename, "JPG")
        print("file %s saved" % (filename))
        self.labelLastPicture.setText("Derniere photo prise : "+name)
        #self.video_frame.setStyleSheet("border: 3px solid red"); # ne fonctionne pas

        # Temporisation + relance du flux
        time.sleep(2)
        self.thread.start()
        self.video_frame.setStyleSheet("border: 1px solid black"); # Inutile


    def print_stream(self):
        fullName = self.labelLastPicture.text()
        pictureName = fullName.split(":")[1][1:]

        self.printer(pictureName)
        print("Last picture from streaming is printing (%s)" % pictureName)

    def print_picture(self):
        pictureName = self.labelRuningPicture.text()
        self.printer(pictureName)
        print("Runing picture is printing (%s)" % pictureName)

    def gotoGallery(self):
        self.tabWidget.setCurrentIndex(1)

    def gotoStreaming(self):
        self.tabWidget.setCurrentIndex(0)

    def refreshFA(self):
        #self.btnRefresh.setEnabled(False)
        getFAdatas()
        self.tabSelect()
        #self.btnRefresh.setEnabled(True)
        print("Datas refreshed")


    def nextGallery(self):
        print("next")
        global listePhotos
        index_tmp = str(self.labelPicture.text())
        split_index = int(index_tmp.split('/')[0]) - 1
        max_index = int(index_tmp.split('/')[1]) - 1

        if split_index == max_index:
            split_index = 0
        else:
            split_index += 1
        self.affiche_gallerie(split_index)

    def previousGallery(self):
        print("previous")
        global listePhotos
        index_tmp = str(self.labelPicture.text())
        split_index = int(index_tmp.split('/')[0]) - 1
        max_index = int(index_tmp.split('/')[1]) - 1

        if split_index == 0:
            split_index = max_index
        else:
            split_index -= 1
        self.affiche_gallerie(split_index)


    def tabSelect(self):
        if self.tabWidget.currentIndex() == 0:
            print("Streaming mode")
        else:
            print("Gallery mode")
            global listePhotos
            listePhotos = os.listdir('pictures')
            listePhotos.sort(reverse=True)
            self.affiche_gallerie(0)

    def affiche_gallerie(self, photo_index):
        global listePhotos
        pix = QPixmap("pictures/"+listePhotos[photo_index])
        #pix = pix.scaled(1366,768, QtCore.Qt.KeepAspectRatio)
        pix = pix.scaled(1366,911, QtCore.Qt.KeepAspectRatio)
        self.photo_frame.setPixmap(pix)
        pictureText = str(photo_index+1) + "/" + str(len(listePhotos))
        self.labelPicture.setText(pictureText)
        self.labelRuningPicture.setText(listePhotos[photo_index])

    def eeg(self):
        import random
        import pygame

        files = os.listdir('sound')
        randomfile = files[random.randint(0, len(files)-1)]
        pygame.mixer.init()
        pygame.mixer.music.load('sound/'+randomfile)
        pygame.mixer.music.play()

    def printer(self, mypicture):
        global printedPicture

        if not mypicture in printedPicture:
            # Copie dans le dossier temporaire d'impression
            copyfile('pictures/' + mypicture, 'temprint/temp.jpg')

            # Lancement du script d'impression Bluetooth
            subprocess.Popen("./scriptprint.sh")

            # Conservation du nom de la photo imprimée
            printedPicture.append(mypicture)
        else:
            print("On n'imprime qu'une fois !")


if __name__=="__main__":
    import sys
    app = QApplication(sys.argv)
    app.setStyleSheet("background-color:black;")
    w = interfaceVideo()
    w.showFullScreen()
    app.exec_()
