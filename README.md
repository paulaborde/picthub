PictHub : Centralize and manage event pictures
-----

# About
New organization : Make a real picture box with PS2 controller and use two differents modes (livestream & eventpicture). Add a printing capacity.
This application will be used in fullscreen mode.

# Technical informations
* __init__.py : Main program with UI loading and commands
* get_FApictures.py : Script using nmap to check Toshiba FlashAir SD connected on access point, get pictures list and download it if not ever done
* mainUI.ui : main UI with QT Designer
* scriptprint.sh : Script to print in Bluetooth on LG Pocket Printer
* updateFA.py : just to load get_FApictures library, not really clean
* threadPicthub.py : 1 thread for video streaming (Dev by Philippe Juhel, Icam)
* Antimicro conf : We use Antimicro to map PS2 controler with special keys to command application


# To Do
* It still have some bugs on pictures backup from wifi SD, need to be more tested 