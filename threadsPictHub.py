# -*- coding: utf-8 -*-
# Source : Philippe Juhel - Icam Toulouse - philippe.juhel@icam.fr

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import time
import cv2
from numpy import *
import pygame, sys, os
from pygame.locals import *
from threading import Thread

class ThreadLectureCamera(QThread):

    # Signaux de communication avec l'IHM (voir classe)
    signalAfficherImage = pyqtSignal(ndarray) # Pour afficher une image dans le QLabel nommé video_frame

    def __init__(self, parent=None):
        super(ThreadLectureCamera, self).__init__(parent)

    def run(self):
        """
        Méthode principale du thread. Traite chaque image issue de la camera pour streaming
        """
        sourceVideo = cv2.VideoCapture(0) # Webcam locale 
        #sourceVideo = cv2.VideoCapture(1) # Camera externe

        while not(self.isFinished()):
            ret, img = sourceVideo.read()
            if ret:
                self.signalAfficherImage.emit(img)
            else:
                print("Pas d'image acquise")
