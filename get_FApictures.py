# -*- coding: utf-8 -*-
import nmap
import urllib2
from os import listdir
import wget
from PIL import Image
from datetime import datetime
from shutil import copyfile

def getPictureList(url, ip):
    tempList = []

    page = urllib2.urlopen(url)
    lines = page.readlines()

    for line in lines:
        if line[0]=='/':
            splitLine = line.split(',')
            if ip!=0:
                tempList.append(splitLine[0]+'/'+splitLine[1])
                tempIP = (str(ip), splitLine[0]+'/'+splitLine[1])
            else:
                tempList.append(splitLine[0]+'/'+splitLine[1])
                tempIP = 0
    return(tempList, tempIP)


def getFAdatas():
    # Test connection SD
    nm = nmap.PortScanner()
    nm.scan(hosts='192.168.1.0/24', arguments='-sP')

    listeHosts = nm.all_hosts()

    # Liste photos
    listePhotos=[]
    dictIP={}

    for sd in listeHosts: # Parcours l'ensemble des machines connectées à l'AP
        if sd != '192.168.1.1':
            print("IP ALIVE : %s" % sd)

            urlDir= 'http://' + sd + '/command.cgi?op=100&DIR=/DCIM'
            dirList, tempIP = getPictureList(urlDir, 0)
            for dir in dirList: # Pour chaque dossier présent sur la SD on liste les sous-dossiers
                urlSubDir = 'http://' + sd + '/command.cgi?op=100&DIR='+dir
                newPhoto, tempIP = getPictureList(urlSubDir, sd)
                listePhotos.extend(newPhoto) # On stock la liste des photos du sous-dossier

                for photo in newPhoto: # Nouvelle boucle obligatoire pour associer l'IP à chaque photo dans un dictionnaire
                    dictIP[photo] = sd
    print("Dict IP : ")
    print(dictIP)
    print("")

    # Compare existant
    listeRenamed = [x.replace('/', '-') for x in listePhotos]
    listeExistant = listdir('fa_temp')
    listeExistant = ['-'+x for x in listeExistant]

    deltafiles = list(set(listeRenamed) - set(listeExistant))

    print("DELTA : ")
    print(deltafiles)
    print("")


    # Télécharge nouvelles photos dans le dossier temporaire et copie dans le dossier de la gallerie
    deltaDownloadable = [x.replace('-', '/') for x in deltafiles]

    for file in deltaDownloadable:
        fileNewName = file.replace('/', '-')[1:]
        ip = dictIP[file]


        outputName = 'fa_temp/'+fileNewName


        print("Téléchargement du fichier : %s" % outputName)
        print("")
        wget.download(url='http://'+ip+'/'+file, out=outputName)
        fileDate = datetime.strptime(Image.open(outputName)._getexif()[36867], '%Y:%m:%d %H:%M:%S')
        copyName = 'pictures/' + fileDate.strftime("%Y-%m-%d_%H-%M-%S") + '_FA-SD.jpg'

        copyfile(outputName, copyName)

    print ('Mise à jour des photos finalisée')
